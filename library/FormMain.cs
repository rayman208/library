﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace library
{
    public partial class FormMain : Form
    {
        MySqlConnection connection;
        MySqlCommand command;
        
        private void RefreshDgvBookRecords()
        {
            command.CommandText = "SELECT * FROM book_records";
            MySqlDataReader reader = command.ExecuteReader();

            dataGridViewBookRecords.Rows.Clear();
            while(reader.Read()==true)
            {
                DateTime date = reader.GetDateTime("date");
                string readerName = reader.GetString("reader_name");
                string bookName = reader.GetString("book_name");
                int term = reader.GetInt32("term");
                int id = reader.GetInt32("id");

                dataGridViewBookRecords.Rows.Add(date, readerName, bookName, term, id);
            }

            reader.Close();
        }        

        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            string connectionParameters = "Server=localhost;Database=library;Port=3306;User=root;Password=1234";
            connection = new MySqlConnection(connectionParameters);
            connection.Open();

            command = new MySqlCommand();
            command.Connection = connection;

            RefreshDgvBookRecords();
        }

        private void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            connection.Close();
        }

        private void buttonInsertRecord_Click(object sender, EventArgs e)
        {
            string date = dateTimePickerDate.Value.ToString("yyyy-MM-dd H:mm");
            string readerName = textBoxReaderName.Text;
            string bookName = textBoxBookName.Text;
            int term = (int)numericUpDownTerm.Value;

            command.CommandText = $"INSERT INTO book_records (date,reader_name,book_name,term) VALUES('{date}','{readerName}','{bookName}',{term})";

            command.ExecuteNonQuery();

            RefreshDgvBookRecords();

            dateTimePickerDate.Value = DateTime.Now;
            textBoxReaderName.Clear();
            textBoxBookName.Clear();
            numericUpDownTerm.Value = 1;
        }

        private void buttonDeleteRecord_Click(object sender, EventArgs e)
        {
            int id = int.Parse(dataGridViewBookRecords.SelectedRows[0].Cells[4].Value.ToString());

            command.CommandText = $"DELETE FROM book_records WHERE id={id}";

            command.ExecuteNonQuery();

            RefreshDgvBookRecords();

        }
    }
}
